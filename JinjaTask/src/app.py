from flask import Flask, jsonify, render_template

from jinja2 import Environment, FileSystemLoader
import os
import json

app = Flask(__name__)

@app.route("/", methods=["GET"])
def ping():
    return jsonify({"response": "pong!"})

# @app.route("/users")
# def usershandler():
#     return jsonify({"users": users})

@app.route("/test")
def testhandler():
    generateHTML()
    return render_template("index.html")

def generateHTML():
    root = os.path.dirname(os.path.abspath(__file__))
    templates_dir = os.path.join(root, 'templates')
    env = Environment( loader = FileSystemLoader(templates_dir) )
    template = env.get_template('templateIndex.html')

    places = getJSON(os.path.join(root, 'data', 'Sample_data.json'))
    print(places.get('recommended_products')[0])

    filename = os.path.join(root, 'templates', 'index.html')
    with open(filename, 'w') as fh:
        fh.write(template.render(
            geoImage = places.get('geo_image'),
            recommendedProducts = places.get('recommended_products'),
        ))
def getJSON(root):
    with open(root) as content:
        return json.load(content)
    return null
        

# Start the Server
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=3000, debug=True)
