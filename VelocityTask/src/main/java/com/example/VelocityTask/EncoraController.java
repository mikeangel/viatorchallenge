package com.example.VelocityTask;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import com.google.gson.Gson;

import java.io.File;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.FileWriter;

@Controller
public class EncoraController {
    @GetMapping("/encora")
	public String encora(String name, Model model) {
		model.addAttribute("name", name);

		VelocityEngine velocityEngine = InitVelocity();
		VelocityContext velocityContext = InitContext();
		Template template = velocityEngine.getTemplate("templates/encora.vm");
		GenerateTemplate(template, velocityContext);
		return "encora";
	}

	public VelocityEngine InitVelocity(){
		VelocityEngine engine = new VelocityEngine();
		engine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		engine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());

		engine.init();

		return engine;
	}

	public VelocityContext InitContext(){
		Place place = new Place();
		try{
			Reader reader =  Files.newBufferedReader(Paths.get("src/main/resources/static/Sample_data.json"));
			Gson gson = new Gson();
			place=  gson.fromJson(reader, Place.class);
		}catch(Exception e){
			System.out.println(e);
		}

		VelocityContext context = new VelocityContext();
		context.put("geo_image", place.geoImage());
		context.put("recommended_products", place.recomendProducts());

		return context;
	}

	public void GenerateTemplate(Template template, VelocityContext context){
		try{
			FileWriter fw = new FileWriter(new File("src/main/resources/templates/encora.html"));
			template.merge(context, fw);
			fw.flush();
			fw.close();
		}
		catch(Exception e){
			System.out.println(e);
		}
	}
}
