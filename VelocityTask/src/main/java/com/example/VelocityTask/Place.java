package com.example.VelocityTask;

public class Place {
    String geo_id;
    String geo_name;
    String geo_image;
    String geo_link;
    Product recommended_products[];

    public Place(){

    }
    public Place(String geoId, String geoName, String geoImage, String geoLink, Product[] recomendProducts){
        geo_id = geoId;
        geo_name = geoName;
        geo_image = geoImage;
        geo_link = geoLink;
        recommended_products = recomendProducts;
    }

    public String geoId(){
        return geo_id;
    }
    public void geoId(String geoId){
        geo_id = geoId;
    }
    public String geoName(){
        return geo_name;
    }
    public void geoName(String geoName){
        geo_name = geoName;
    }
    public String geoImage(){
        return geo_image;
    }
    public void geoImage(String geoImage){
        geo_image = geoImage;
    }
    public String geoLink(){
        return geo_link;
    }
    public void geoLink(String geoLink){
        geo_link = geoLink;
    }
    public Product[] recomendProducts(){
        return recommended_products;
    }
    public void recomendProducts(Product[] recomeProducts){
        recommended_products = recomeProducts;
    }
}
