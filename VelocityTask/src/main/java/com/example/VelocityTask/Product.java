package com.example.VelocityTask;

public class Product {
    String product_code;
    String product_name;
    String product_rating;
    String product_photo_url;
    String product_link;
    String product_review_count;
    public Product(){

    }
    public Product(String productCode, String productName, String productRating, String productPhoto_url, String productLink, String productReview_count){
        product_code = productCode;
        product_name = productName;
        product_rating = productRating;
        product_photo_url = productPhoto_url;
        product_link = productLink;
        product_review_count = productReview_count;
    }

    public String productCode(){
        return product_code;
    }
    public String productName(){
        return product_name;
    }
    public String productRating(){
        return product_rating;
    }
    public String productPhoto_url(){
        return product_photo_url;
    }
    public String productLink(){
        return product_link;
    }
    public String productReview_count(){
        return product_review_count;
    }

    public void ProductCode(String data){
        product_code = data;
    }
    public void ProductName(String data){
        product_name = data;
    }
    public void ProductRating(String data){
        product_rating = data;
    }
    public void ProductPhoto(String data){
        product_photo_url = data;
    }
    public void ProductLink(String data){
        product_link = data;
    }
    public void ProductReview(String data){
        product_review_count = data;
    }
    public String getUrlRating(String value){
        switch(value) {
            case "1.5" :
                return "https://cdn.tripadvisor.com/img2/email/rex/ratingstar1.5.png";
            default:
                return "https://cdn.tripadvisor.com/img2/email/rex/ratingstar1.0.png";
        }   
    }
}
