package com.example.VelocityTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VelocityTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(VelocityTaskApplication.class, args);
	}

}
